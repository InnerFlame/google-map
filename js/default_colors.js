/**
 *
 * default_color['Parks'] inside we use first word of category(if category contains from couple words)
 */

var default_color = [];
 default_color['Parks']       = "#fafafa";
 default_color['Police']      = "#F0380E";
 default_color['Grocery']     = "#02F5C0";
 default_color['Montessori']  = "#0227F5";
 default_color['Community']   = "#0BF502";
 default_color['Catholic']    = "#F502B4";
 default_color['Public']      = "#F58C02";

default_color['default'] = '#c48a8a';
