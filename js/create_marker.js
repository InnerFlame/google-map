/**
 * Created by linux on 27.03.15.
 */
var gmarkers = [];
var gicons = [];
var map = null;

function getDecimal(num) {

    intValue = parseInt(num);

    floatValue = num - intValue;

    return +floatValue.toFixed(6);

}

function onlyUnique(value, index, self) {

    return self.indexOf(value) === index;

}

var infowindow = new google.maps.InfoWindow(
    {
        size: new google.maps.Size(150,50)
    });

// A function to create the marker and set up the event window
function createMarker(id, latlng,title,html,category, icon,address,description,website,rating, short) {
    var contentString = html;
    //var marker = new google.maps.Marker({
    //    position: latlng,
    //    icon: icon,//gicons[category],
    //
    //    map: map,
    //    title: title
    //
    //});
    var marker = new google.maps.Marker({position:latlng,icon: icon});


    marker.setVisible(false);

    // ===Store the category and name info as a marker properties ===

    marker.mycategory = category;
    marker.short_category = short;
    marker.title = title;
    marker.address = address;
    marker.description = description;
    marker.rating = rating;
    marker.website = website;
    marker.id = id;
    gmarkers.push(marker);

////////////////////////////////////////////////////////////////////////////////////

    google.maps.event.addListener(marker, 'click', function() {
        map.panTo(marker.position);
        infowindow.setContent(contentString);
        infowindow.open(map,marker);
    });

    google.maps.event.addListener(map, 'click', function () {
        infowindow.close();
    });

    ////////////////////////////////////////////////////////////////////
//
//    google.maps.event.addListener(map, 'dragend', function(){
//        num_results = 0;
//        //console.log(gmarkers);
//        for(var i in gmarkers){
//            if(map.getBounds().contains(gmarkers[i].position)){
//                $('#' + gmarkers[i].id).show(600);
//                num_results++;
//            }else{
//                $('#' + gmarkers[i].id).hide(600);
//            }
//        }
//    });
//
///////////////////////////////////////////////////////////////////////////////////////
//
//    google.maps.event.addListener(map, 'zoom_changed', function() {
//        //console.log('zoom_changed ' + map.getZoom());
//        num_results = 0;
//        for (var i in gmarkers) {
//            if (map.getBounds().contains(gmarkers[i].position)) {
//                $('#' + gmarkers[i].id).show(600);
//                num_results++;
//            } else {
//                $('#' + gmarkers[i].id).hide(600);
//            }
//        }
//    });

//////////////////////////////////////////////////////////////////////////////
}

