/**
 * filter
 */

function filter_rating_button(){
    var filtered = [];
    for(i in gmarkers){
        filtered[i] = gmarkers[i].rating;
    }
    //console.log(sort_rating);
    var filter =[];

//////////////////////////////first variant filter//////////////////////////////////////////////////////////////////

    if(window.sort_rating) {                    //two variant about sort for rating

        //console.log(0);
        window.sort_rating = false;

        filter = filtered.map(function (rating, index) {
            return {index: index, rating: rating};
        }).sort(function (fruit1, fruit2) {
            return fruit1.rating < fruit2.rating ? -1 : fruit1.rating > fruit2.rating ? 1 : 0;
        });

        makeSidebar(filter);
        //////////////////////////////sec0nd variant filter//////////////////////////////////////////////////////////////////

    }else{                                      ////two variant about sort for rating

        window.sort_rating = true;

        //console.log(1);

        filter = filtered.map(function (rating, index) {
            return {index: index, rating: rating};
        }).sort(function (fruit1, fruit2) {
            return fruit1.rating < fruit2.rating ? 1 : fruit1.rating > fruit2.rating ? -1 : 0;
        });

        makeSidebar(filter);
    }
//////////////////////////////sec0nd variant filter//////////////////////////////////////////////////////////////////

}





function filter_rating_title() {

    //////////////////founding right of list from a-z//////////////////
    var listNew = [];
    for (var i in gmarkers) {
        listNew.push({id: i, title: gmarkers[i].title});
    }

/////////////////first variant filter///////////////////////////////////////////////////////////////////
    if (window.sort_rating) {                    //two variant about sort for rating

        //console.log(0,listNew);

        window.sort_rating = false;

        function compareObjects(a, b) {
            if (a.title > b.title) return  1;
            if (a.title < b.title) return -1;
            return 0;
        };
        listNew.sort(compareObjects);
        //////////////////////////////sec0nd variant filter//////////////////////////////////////////////////////////////////
    } else {

        //console.log(1,listNew);

        window.sort_rating = true;

        function compareObjects2(a, b) {
            if (a.title > b.title) return -1;
            if (a.title < b.title) return  1;
            return 0;
        }
        listNew.sort(compareObjects2);
    }
/////////////////////////////////////////////////////////////////////////////////////

    //console.log(listNew);


///////////////////////////////////////////////

    var html = header_of_table;

    var z = 0;
    for (var i in gmarkers) {

        /////////////////////////////for filter by rating       ////////////////////////////////////////////

            for (var j in gmarkers) {
                if (gmarkers[j].title == listNew[z].title) { // write good title by a-z
                    i = j;//console.log(listNew[z].title);
                }
        }
        z++;
///////////////////////////////////////////////////////////////////////////////////

        if(gmarkers[i].website =='') gmarkers[i].site = '';
        else                         gmarkers[i].site =  "<a href='http://" + gmarkers[i].website +
                                                         "' target='_blank'>link</a> ";

        if (gmarkers[i].getVisible()) {

            var input_color = '';//"<input  value='"+category2color(gmarkers[i].short_category)     +
            //                  "' onchange=save_to_Coockie()" +
            //                  " id='"+ gmarkers[i].short_category + "' type='color' />" ;

            // minus = gmarkers[i-1] || [];//color  only once will be in one category
            // if(gmarkers[i].mycategory == minus.mycategory)input_color = '';

            html +=
                "<tr id='"+gmarkers[i].id+"'>" +
                "<td><a href='javascript:myclick(" + i + ")'>" + gmarkers[i].title + "<\/a><br></td>" +
                "<td>" + gmarkers[i].description + " </td> " +
                "<td>" + gmarkers[i].address + "</td> " +
                "<td> " + gmarkers[i].site + "</td> " +
                "<td> "+ gmarkers[i].mycategory + " </td> " + //<td >"+input_color +" </td>
                "<td>" + gmarkers[i].rating + "</td>" +
                "</tr>";
        }
    }
    html +='</table>';
    document.getElementById("side_bar").innerHTML = html;
}
