$(document).ready(function(){
    var pac_input = (document.getElementById('adress'));
    var autocomplete = new google.maps.places.Autocomplete(pac_input);


    google.maps.event.addListener(autocomplete, 'place_changed', function() {
        infowindow.close();
        //marker.setVisible(false);
        var place = autocomplete.getPlace();
        if (!place.geometry) {
            return;
        }
        // If the place has a geometry, then present it on a map.
        if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);

        } else {
            map.setCenter(place.geometry.location);
            map.setZoom(17);  // Why 17? Because it looks good.
        }
        if(map.getZoom()>13) map.setZoom(13); //let us set zoom to 13 if zoom >13
        auto_input=pac_input.value; //set place value to input box when hit pac-input-button
        $('#postal').val(pac_input.value);
    }); //pac-input function
});


$(document).ready(function() {
    $("#pac-input").focusin(function () {
        $(document).keypress(function (e) {
            if (e.which == 13) {
                $('#pac-input').val('');
            }
        });
    });//pac-input box
    $("#pac-input-button").click(function () {
        $('#pac-input').val(auto_input);
        auto_input = "";
    });//pac-input button
});